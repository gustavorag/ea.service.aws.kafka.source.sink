package fusemail;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

@RunWith(JUnit4.class)
public class ExampleTest {

    @Test
    public void testExample() {
        Assert.assertNotEquals("example unit test assertion", "expected", "actual");
    }
}