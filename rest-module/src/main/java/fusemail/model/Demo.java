package fusemail.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Demo {

    @JsonProperty("sample_text")
    private String sampleText = "";

    public String getSampleText() {
        return sampleText;
    }

    public void setSampleText(String sampleText) {
        this.sampleText = sampleText;
    }
}
