package fusemail;


import com.fusemail.commons.debug.DebugLog;
import com.fusemail.commons.metrics.Metrics;
import com.fusemail.commons.monitoring.MonitoringInfo;
import com.fusemail.commons.properties.AbstractProperties;
import com.fusemail.commons.rest.ApiDocRequestHandler;
import com.fusemail.commons.rest.DebugRequestHandler;
import com.fusemail.commons.rest.MonitoringRequestHandler;
import com.fusemail.commons.rest.ReadMeRequestHandler;
import com.fusemail.commons.rest.VersionRequestHandler;
import fusemail.util.AppTemplateProperties;
import fusemail.version.Version;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LoggerContext;
import org.glassfish.grizzly.http.server.HttpServer;
import org.glassfish.jersey.grizzly2.httpserver.GrizzlyHttpServerFactory;
import org.glassfish.jersey.jackson.JacksonFeature;
import org.glassfish.jersey.server.ResourceConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.bridge.SLF4JBridgeHandler;

import java.net.URI;


public final class Application {
    private static final Logger logger = LoggerFactory.getLogger(Application.class);

    private static final AbstractProperties properties = AppTemplateProperties.getInstance();

    // base URL
    private static final String BASE_URL = properties.getRequiredProperty("baseURL");

    // Dependency check refresh
    private static final int MONITOR_REFRESH_INTERVAL_SECONDS = properties.getIntValueWithDefault("monitor.refreshTimeSeconds", 20);

    // Debug Log Config
    private static final DebugLog.Config DEBUG_LOG_CONFIG = new DebugLog.Config()
            .withDefaultDurationMin(properties.getRequiredIntProperty("debug.defaultDurationMin"))
            .withBufferSize(properties.getRequiredIntProperty("debug.bufferSize"))
            .withTextLayout(properties.getRequiredProperty("debug.textPatternLayout"))
            .withDumpOnCrash(Boolean.parseBoolean(properties.getRequiredProperty("debug.dumpOnCrash")));

    // metrics initialization
    private static final String METRICS_LIBRARY_STRING = properties.getProperty("metricsLibrary", "none");
    private static final Metrics.MetricLibrary METRIC_LIBRARY = Metrics.MetricLibrary.fromValue(METRICS_LIBRARY_STRING);
    private static final Metrics METRICS = Metrics.init(METRIC_LIBRARY);

    private static final String[] RESOURCE_PACKAGES = new String[]{
            "com.fusemail.interceptor",
            "com.fusemail.rest"
    };

    private static final String PROJECT_NAME = "ea.service.aws.kafka.source.sink";
    private static final String README_RESOURCE= "README.md";
    private static final String API_DOC_RESOURCE = "index.html";

    static {
        SLF4JBridgeHandler.removeHandlersForRootLogger();
        SLF4JBridgeHandler.install();
    }

    private Application() {}

    public static void main(String[] args) {
        try {
            LoggerContext context = LoggerContext.getContext();
            context.getConfiguration().getRootLogger().setLevel(Level.INFO);
            logger.info("Starting service.");

            MonitoringInfo monitoringInfo = getMonitoringInfo();
            if (!monitoringInfo.start()) {
                logger.error("Failed health check");
                System.exit(-1);
            }

            ResourceConfig resourceConfig = new ResourceConfig()
                    .packages(RESOURCE_PACKAGES)
                    .register(new MonitoringRequestHandler(monitoringInfo))
                    .register(new VersionRequestHandler(Version.getVersionInfo()))
                    .register(METRICS.getMetricsRequestHandler())
                    .register(new ReadMeRequestHandler(PROJECT_NAME, README_RESOURCE))
                    .register(new ApiDocRequestHandler(API_DOC_RESOURCE))
                    .register(new DebugRequestHandler(DEBUG_LOG_CONFIG))
                    .register(JacksonFeature.class);

            HttpServer httpServer = GrizzlyHttpServerFactory.createHttpServer(URI.create(BASE_URL), resourceConfig);
            httpServer.start();

            logger.info("Application started: {}", BASE_URL);
        } catch (Exception e) {
            logger.error("Failed to start the application! Exception: {}", e.getMessage(), e);
            System.exit(-1);
        }
    }

    private static MonitoringInfo getMonitoringInfo() {
        return new MonitoringInfo(
            Version.getVersionInfo(),
            MONITOR_REFRESH_INTERVAL_SECONDS
        );
    }
}
