package fusemail.rest;

import fusemail.model.Demo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/rest/demo")
public class DemoRestImpl {

    private static final Logger logger = LoggerFactory.getLogger(DemoRestImpl.class);

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response demoMethod() {
        Demo demo = new Demo();
        demo.setSampleText("hello world!");

        logger.error("test error");
        logger.info("test info");
        logger.debug("test debug");

        return Response.ok(demo).build();
    }
}
