package fusemail.version;


import com.fusemail.commons.version.VersionInfo;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public final class Version {

    private static final String RELEASE = "1.0";
    private static final String BUILD_TIME = "2021-08-17T01:03:29Z";
    private static final String GIT_HASH = "8a10822f1014d1f4607260e8f1f65874b13c5b57";

    private static final VersionInfo versionInfo = new VersionInfo(
            RELEASE,
            ZonedDateTime.from(DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:sszzz").parse(BUILD_TIME)),
            GIT_HASH
    );

    public static VersionInfo getVersionInfo() {
        return  versionInfo;
    }

    private Version() {}
}
