package fusemail.interceptor;


import fusemail.util.JSONLayoutLogs;
import fusemail.util.MetricsUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class DefaultExceptionHandler implements ExceptionMapper<Throwable> {
    private static final Logger logger = LoggerFactory.getLogger(DefaultExceptionHandler.class);

    @Override
    public Response toResponse(Throwable throwable) {
        AuditEntry auditEntry = AuditEntry.getLocalEntry();
        if (MetricsUtil.UNKNOWN_URL_TAG.equalsIgnoreCase(auditEntry.getRestUrlTemplate())) {
            // if rest url template is unknown, we need to log the actual url since it does not
            // go through the post matching filter.
            MDC.put(JSONLayoutLogs.REQUEST_URL_KEY, auditEntry.getActualUrl());
            MDC.put(JSONLayoutLogs.REQUEST_HTTP_METHOD_KEY, auditEntry.getHttpMethod());
        }

        logger.info("error: {}", throwable.getMessage());
        if (logger.isDebugEnabled()) {
            logger.info("error: {}", throwable.getMessage(), throwable);
        }

        MDC.remove(JSONLayoutLogs.REQUEST_URL_KEY);
        MDC.remove(JSONLayoutLogs.REQUEST_HTTP_METHOD_KEY);
        // ip is being stored at the pre-matching process and might not get removed properly
        MDC.remove(JSONLayoutLogs.REQUEST_IP_KEY);

        Response.Status httpStatus = Response.Status.INTERNAL_SERVER_ERROR;
        String errorMessage = throwable.getMessage();

        if (throwable instanceof NotFoundException) {
            httpStatus = Response.Status.NOT_FOUND;
        }

        Map<String, String> error = new HashMap<>();
        error.put("error", errorMessage);

        return Response
            .status(httpStatus)
            .entity(error)
            .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
            .build();
    }
}
