package fusemail.interceptor;


public class AuditEntry {

    private long startTimeNanoseconds = 0;
    private double responseTimeSeconds = 0D;
    private String restUrlTemplate = "";
    private Integer httpStatus = null;
    private String actualUrl = "";
    private String httpMethod = "";

    private static final double NANOSECONDS_IN_SECONDS = 1_000_000_000D;

    private static ThreadLocal<AuditEntry> localEntry = new ThreadLocal<>();

    /**
     * Get the AuditEntry for the current request
     *
     * @return AuditEntry for the current request
     */
    public static synchronized AuditEntry getLocalEntry() {
        return localEntry.get();
    }

    /**
     * Set the AuditEntry for the current request
     *
     * @param auditEntry AuditEntry for the current request
     */
    public static synchronized void setLocalEntry(AuditEntry auditEntry) {
        localEntry.set(auditEntry);
    }

    public static synchronized void clearLocalEntry() {
        localEntry.remove();
    }

    public void startTimer() {
        this.startTimeNanoseconds = System.nanoTime();
    }

    public void stopTimer() {
        long currentTimeNanoseconds = System.nanoTime();
        long elapsedTimeNanoseconds = currentTimeNanoseconds - startTimeNanoseconds;
        this.responseTimeSeconds = elapsedTimeNanoseconds / NANOSECONDS_IN_SECONDS;
    }

    public double getResponseTimeSeconds() {
        return responseTimeSeconds;
    }

    public String getRestUrlTemplate() {
        return restUrlTemplate;
    }

    public void setRestUrlTemplate(String restUrlTemplate) {
        this.restUrlTemplate = restUrlTemplate;
    }

    public Integer getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(Integer httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getActualUrl() {
        return actualUrl;
    }

    public void setActualUrl(String actualUrl) {
        this.actualUrl = actualUrl;
    }

    public String getHttpMethod() {
        return httpMethod;
    }

    public void setHttpMethod(String httpMethod) {
        this.httpMethod = httpMethod;
    }
}
