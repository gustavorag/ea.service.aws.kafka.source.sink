package fusemail.interceptor;


import fusemail.util.JSONLayoutLogs;
import fusemail.util.MetricsUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.io.IOException;

import javax.ws.rs.Path;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.ext.Provider;


@Provider
public class RequestInterceptor implements ContainerRequestFilter{
    private static final Logger logger = LoggerFactory.getLogger(RequestInterceptor.class);

    @Context
    private ResourceInfo resourceInfo;

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        // retrieve statistics entry for the request
        AuditEntry auditEntry = AuditEntry.getLocalEntry();
        auditEntry.setRestUrlTemplate(getUrlTemplate());

        // record start request stats
        MetricsUtil.recordStartRequest(auditEntry);

        // logging template url and overwrite the actual url
        MDC.put(JSONLayoutLogs.REQUEST_URL_KEY, auditEntry.getRestUrlTemplate());
        // logging the http method
        MDC.put(JSONLayoutLogs.REQUEST_HTTP_METHOD_KEY, auditEntry.getHttpMethod());

        MultivaluedMap<String, String> pathParams = requestContext.getUriInfo().getPathParameters();
        if (pathParams != null && !pathParams.isEmpty()) {
            pathParams.forEach((key, value) -> MDC.put(
                JSONLayoutLogs.PATH_PARAM_KEY_PREFIX + key,
                value.toString()));
        }

        logger.info("Request received.");

        MDC.remove(JSONLayoutLogs.REQUEST_URL_KEY);
        MDC.remove(JSONLayoutLogs.REQUEST_IP_KEY);
        MDC.remove(JSONLayoutLogs.REQUEST_HTTP_METHOD_KEY);

        if (pathParams != null && !pathParams.isEmpty()) {
            pathParams.forEach((key, value) -> MDC.remove(
                JSONLayoutLogs.PATH_PARAM_KEY_PREFIX + key));
        }

    }

    /**
     * Get url template from Path annotation from both at the class level and method level.
     */
    private String getUrlTemplate() {
        Path methodPath = resourceInfo.getResourceMethod().getAnnotation(Path.class);
        Path classPath = resourceInfo.getResourceClass().getAnnotation(Path.class);

        // if class path annotation is null, lets try to get it from its interfaces
        if (classPath == null) {
            Class<?>[] classInterfaces = resourceInfo.getResourceClass().getInterfaces();
            for (Class<?> classInterface: classInterfaces) {
                Path interfacePath = classInterface.getAnnotation(Path.class);
                if (interfacePath != null) {
                    classPath = interfacePath;
                    break;
                }
            }
        }

        // if it's still null, we will just give up and assign to unknown rest path.
        if (classPath != null) {
            if (methodPath != null) {
                return String.format("%s/%s", classPath.value(), methodPath.value());
            }

            return classPath.value();
        }

        return "";
    }

}
