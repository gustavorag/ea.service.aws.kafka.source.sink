package fusemail.interceptor;

import fusemail.util.JSONLayoutLogs;
import fusemail.util.MetricsUtil;

import org.glassfish.grizzly.http.server.Request;
import org.glassfish.jersey.server.ContainerRequest;
import org.slf4j.MDC;

import java.util.UUID;

import javax.inject.Inject;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.PreMatching;
import javax.ws.rs.ext.Provider;

/**
 * Log timing in all request as well as assigned unique request id.
 * This filter is called before url matching step.
 */
@Provider
@PreMatching
public class AuditRequestInterceptor implements ContainerRequestFilter {

    private javax.inject.Provider<Request> grizzlyRequest;

    @Inject
    public AuditRequestInterceptor(javax.inject.Provider<Request> grizzlyRequest) {
        this.grizzlyRequest = grizzlyRequest;
    }

    @Override
    public void filter(ContainerRequestContext requestContext) {
        AuditEntry auditEntry = new AuditEntry();
        AuditEntry.setLocalEntry(auditEntry);
        auditEntry.startTimer();
        auditEntry.setRestUrlTemplate(MetricsUtil.UNKNOWN_URL_TAG);
        auditEntry.setActualUrl("/" + ((ContainerRequest) requestContext).getPath(true));
        auditEntry.setHttpMethod(requestContext.getMethod());

        // creating unique UUID for each request
        UUID uuid = UUID.randomUUID();
        MDC.put(JSONLayoutLogs.REQUEST_UUID_KEY, uuid.toString());

        // logging incoming request ip
        MDC.put(JSONLayoutLogs.REQUEST_IP_KEY, grizzlyRequest.get().getRemoteAddr());
    }
}
