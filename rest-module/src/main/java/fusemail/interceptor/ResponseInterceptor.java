package fusemail.interceptor;

import fusemail.util.JSONLayoutLogs;
import fusemail.util.MetricsUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.MDC;

import java.io.IOException;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.ext.Provider;

@Provider
public class ResponseInterceptor implements ContainerResponseFilter {
    private static final Logger logger = LoggerFactory.getLogger(ResponseInterceptor.class);

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        AuditEntry auditEntry = AuditEntry.getLocalEntry();
        if (auditEntry != null) {
            auditEntry.stopTimer();
            auditEntry.setHttpStatus(responseContext.getStatus());

            // log to metrics
            MetricsUtil.recordEndRequest(auditEntry);

            // add tag to log
            MDC.put(JSONLayoutLogs.RESPONSE_TIME_KEY, Double.toString(auditEntry.getResponseTimeSeconds()));
            MDC.put(JSONLayoutLogs.HTTP_STATUS_KEY, auditEntry.getHttpStatus().toString());
        }

        logger.info("Response sent");

        MDC.remove(JSONLayoutLogs.RESPONSE_TIME_KEY);
        MDC.remove(JSONLayoutLogs.HTTP_STATUS_KEY);
        MDC.remove(JSONLayoutLogs.REQUEST_UUID_KEY);
        AuditEntry.clearLocalEntry();
    }
}
