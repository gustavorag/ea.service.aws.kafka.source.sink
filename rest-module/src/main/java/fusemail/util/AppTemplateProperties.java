package fusemail.util;


import com.fusemail.commons.properties.AbstractProperties;
import fusemail.version.Version;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public final class AppTemplateProperties extends AbstractProperties {
    private static final Logger logger = LoggerFactory.getLogger(AppTemplateProperties.class);

    private static final String APP_TEMPLATE_PROPERTIES_PREFIX = "apptemplate";
    private static final String PROPERTIES_FILE_PATH = System.getProperty("apptemplate.properties", "config/apptemplate.properties");

    private static AppTemplateProperties instance = new AppTemplateProperties();

    private AppTemplateProperties() {}

    public static AppTemplateProperties getInstance() {
        return instance;
    }

    @Override
    protected void printMessages(List<String> messages) {
        messages.forEach(logger::debug);
    }

    @Override
    public String getPrefix() {
        return APP_TEMPLATE_PROPERTIES_PREFIX;
    }

    @Override
    public boolean isDebug() {
        return Boolean.valueOf(getProperty("isDebug", "false"));
    }

    @Override
    public String getVersion() {
        return Version.getVersionInfo().getVersion();
    }

    @Override
    protected String getPropertiesPath() {
        return PROPERTIES_FILE_PATH;
    }
}
