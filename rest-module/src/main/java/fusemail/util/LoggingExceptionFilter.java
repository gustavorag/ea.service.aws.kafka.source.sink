package fusemail.util;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.LogEvent;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;
import org.apache.logging.log4j.core.filter.AbstractFilter;


/**
 * Created by Ken Hargreaves on November 4, 2016.
 *
 * Rewritten to support log4j2
 * Example code from http://logging.apache.org/log4j/2.x/manual/extending.html#Filters
 * simple example code from fm-api-messagelogs
 *
 * This class intercepts ERROR and WARN log levels and increments the counter in statsd.
 *
 * It does not make any decision on whether to log the event or not.
 *
 * To activate this filter it must be part of the configuration file
 *    <LoggingExceptionFilter />  needs to be added within the Appender.
 *
 */
@Plugin(name = "LoggingExceptionFilter", category = "Core", elementType = "filter", printObject = true)
public final class LoggingExceptionFilter extends AbstractFilter {
    private LoggingExceptionFilter() { super(); }


    @Override
    public Result filter(LogEvent event)
    {
        if( event.getLevel() == Level.ERROR ) {
            MetricsUtil.incrementError(event.getLoggerName());
        }

        return Result.NEUTRAL;
    }


    @PluginFactory
    public static LoggingExceptionFilter createFilter() {
        return new LoggingExceptionFilter();
    }
}
