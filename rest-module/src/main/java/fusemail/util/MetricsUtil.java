package fusemail.util;


import com.fusemail.commons.metrics.Counter;
import com.fusemail.commons.metrics.Gauge;
import com.fusemail.commons.metrics.Metrics;
import com.fusemail.commons.metrics.Timer;
import fusemail.interceptor.AuditEntry;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public final class MetricsUtil {

    public static final String UNKNOWN_URL_TAG = "unknown";

    private static final String LOGGER_NAME_LABEL = "logger_name";
    private static final Counter ERROR_COUNTER = Metrics.getInstance().getCounterBuilder()
        .name("error_counter")
        .description("Service error counter")
        .labelNames(Collections.singleton(LOGGER_NAME_LABEL))
        .create();

    private static final String LABEL_KEY_HTTP_STATUS = "http_status";
    private static final String LABEL_KEY_REST_URL = "url";
    private static final Timer REQUEST_TIMER = Metrics.getInstance().getTimerBuilder()
        .name("http_request_duration_seconds")
        .description("Timer for each rest method and its response code in seconds.")
        .labelNames(Stream.of(
            LABEL_KEY_HTTP_STATUS,
            LABEL_KEY_REST_URL).collect(Collectors.toSet()))
        .create();

    private static final Counter REQUEST_COUNTER = Metrics.getInstance().getCounterBuilder()
        .name("http_requests_total")
        .description("Number of requests made to the service.")
        .labelNames(Collections.singleton(LABEL_KEY_REST_URL))
        .create();

    private static final Gauge INFLIGHT_REQUEST_COUNTER = Metrics.getInstance().getGaugeBuilder()
        .name("inflight_http_requests")
        .description("Number of requests in progress in the service.")
        .labelNames(Collections.singleton(LABEL_KEY_REST_URL))
        .create();

    private MetricsUtil() {}

    public static void incrementError(String loggerName) {
        Map<String, String> labelMap = new HashMap<>();
        labelMap.put(LOGGER_NAME_LABEL, loggerName);

        ERROR_COUNTER.increment(labelMap);
    }

    public static void recordStartRequest(AuditEntry entry) {
        Map<String, String> labelMap = new HashMap<>();
        labelMap.put(LABEL_KEY_REST_URL, entry.getRestUrlTemplate());

        INFLIGHT_REQUEST_COUNTER.increment(labelMap);
        REQUEST_COUNTER.increment(labelMap);
    }

    public static void recordEndRequest(AuditEntry entry) {
        Map<String, String> labelMap = new HashMap<>();
        labelMap.put(MetricsUtil.LABEL_KEY_REST_URL, entry.getRestUrlTemplate());

        INFLIGHT_REQUEST_COUNTER.decrement(labelMap);

        labelMap.put(MetricsUtil.LABEL_KEY_HTTP_STATUS, entry.getHttpStatus().toString());
        REQUEST_TIMER.observe(labelMap, entry.getResponseTimeSeconds());
    }

}
