package fusemail.util;


import com.fusemail.commons.logger.JSONLayoutCustom;

import org.apache.logging.log4j.core.Layout;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.Node;
import org.apache.logging.log4j.core.config.plugins.Plugin;
import org.apache.logging.log4j.core.config.plugins.PluginAttribute;
import org.apache.logging.log4j.core.config.plugins.PluginConfiguration;
import org.apache.logging.log4j.core.config.plugins.PluginFactory;

import java.nio.charset.Charset;

@Plugin(name = "JSONLayoutLogs", category = Node.CATEGORY, elementType = Layout.ELEMENT_TYPE, printObject = false)
public final class JSONLayoutLogs extends JSONLayoutCustom {

    public static final String REQUEST_URL_KEY = "url";
    public static final String REQUEST_HTTP_METHOD_KEY = "method";
    public static final String REQUEST_UUID_KEY = "request_uuid";
    public static final String REQUEST_IP_KEY = "request_ip";
    public static final String RESPONSE_TIME_KEY = "response_time_seconds";
    public static final String HTTP_STATUS_KEY = "http_status";
    public static final String PATH_PARAM_KEY_PREFIX = "path_param.";

    private JSONLayoutLogs(Configuration config, boolean locationInfo, boolean properties,
                             boolean encodeThreadContextAsList, boolean complete, boolean compact,
                             boolean eventEol, String headerPattern, String footerPattern, Charset charset) {
        super(config, locationInfo, properties, encodeThreadContextAsList,
            complete, compact, eventEol, headerPattern, footerPattern, charset);
    }

    @PluginFactory
    public static JSONLayoutCustom createLayout(
        @PluginConfiguration final Configuration config,
        @PluginAttribute(defaultBoolean = false, value = "locationInfo") final boolean locationInfo,
        @PluginAttribute(defaultBoolean = false, value = "properties") final boolean properties,
        @PluginAttribute(defaultBoolean = false, value = "propertiesAsList") final boolean propertiesAsList,
        @PluginAttribute(defaultBoolean = false, value = "complete") final boolean complete,
        @PluginAttribute(defaultBoolean = false, value = "compact") final boolean compact,
        @PluginAttribute(defaultBoolean = false, value = "eventEol") final boolean eventEol,
        @PluginAttribute(defaultString = DEFAULT_HEADER, value = "header") final String headerPattern,
        @PluginAttribute(defaultString = DEFAULT_FOOTER, value = "footer") final String footerPattern,
        @PluginAttribute(defaultString = "UTF-8", value = "charset") final Charset charset) {
        final boolean encodeThreadContextAsList = properties && propertiesAsList;
        return new JSONLayoutLogs(config, locationInfo, properties, encodeThreadContextAsList,
            complete, compact, eventEol, headerPattern, footerPattern, charset);
    }

}
