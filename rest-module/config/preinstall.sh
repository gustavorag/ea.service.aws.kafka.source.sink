#!/bin/sh

VENDOR='fusemail'
PROJECT_NAME='ea.service.aws.kafka.source.sink'
ENVDIR="/etc/${VENDOR}/${PROJECT_NAME}/env"

if [ ! -d $ENVDIR ]; then
    mkdir -p $ENVDIR
fi
