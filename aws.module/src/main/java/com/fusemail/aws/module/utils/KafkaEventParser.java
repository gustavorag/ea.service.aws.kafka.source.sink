package com.fusemail.aws.module.utils;

import com.fusemail.common.module.util.JsonUtil;

import com.fasterxml.jackson.core.JsonProcessingException;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class KafkaEventParser {

    public static <T> List<T> parseRecordToClass(Map<String, Object> event, Class<T> clazz) throws JsonProcessingException {
        List<T> values = new ArrayList<>();

        Map<String, List<Map>> records =
                (Map<String, List<Map>>) event.get("records");
        for (Map.Entry<String, List<Map>> entry : records.entrySet()) {

            for (Map entryValue : entry.getValue()) {
                values.add(JsonUtil.fromMapToClass(entryValue, clazz));
            }
        }

        return values;
    }

    public static List<Map> getRecords(Map<String, Object> event) throws JsonProcessingException {
        List<Map> values = new ArrayList<>();

        Map<String, List<Map>> records =
                (Map<String, List<Map>>) event.get("records");
        for (Map.Entry<String, List<Map>> entry : records.entrySet()) {

            for (Map entryValue : entry.getValue()) {
                values.add(entryValue);
            }
        }
        return values;
    }


}
