package com.fusemail.aws.module;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

public class HelloWorld implements RequestHandler<Object, Object> {
    @Override
    public Object handleRequest(Object o, Context context) {

        System.out.println("Hello World from Lambda");
        if(o != null) {
            System.out.println("I've got "+o.toString());
            System.out.println("I've got class: "+o.getClass().getName());
        }
        return null;
    }
}
