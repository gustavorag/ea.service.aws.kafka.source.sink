package com.fusemail.aws.module;

import com.fusemail.aws.module.utils.KafkaEventParser;
import com.fusemail.aws.module.utils.KafkaRecord;
import com.fusemail.kafka.module.sink.ElasticSearchSink;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.util.List;
import java.util.Map;

public class KafkaElasticSearchSink implements RequestHandler<Object, Object> {

    private final static String ELASTICSEARCH_INDEX = "ELASTICSEARCH_INDEX";
    private final static String ELASTICSEARCH_HOST = "ELASTICSEARCH_HOST";
    private final static String ELASTICSEARCH_IP = "ELASTICSEARCH_IP";

    @Override
    public Object handleRequest(Object payload, Context context) {

        String indexName = System.getenv().get(ELASTICSEARCH_INDEX);
        String host = System.getenv().get(ELASTICSEARCH_HOST);
        String ip = System.getenv().get(ELASTICSEARCH_IP);

        System.out.println("Received: " + String.valueOf(payload));
        System.out.println("PaylodType is: " + payload.getClass().getName());

        try {
            ElasticSearchSink sink = new ElasticSearchSink(indexName, host, Integer.valueOf(ip));
            List<KafkaRecord> records = KafkaEventParser.parseRecordToClass((Map) payload, KafkaRecord.class);

            for (KafkaRecord record : records) {
                System.out.println("Converted ConsumerRecord: " + record);
                sink.precessRecord(record.parseToConsumerRecord());
            }

        } catch (Exception e) {
            System.out.println("Error when processing input: " + e.getMessage());
        }


        return null;
    }
}
