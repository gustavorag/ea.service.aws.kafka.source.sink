package com.fusemail.aws.module.utils;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.common.record.TimestampType;

import java.util.Base64;
import java.util.List;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
public class KafkaRecord {

    private String topic;
    private Integer partition;
    private Integer offset;
    private Long timestamp;
    private String timestampType;
    private String key;
    private String value;
    private List<String> headers;

    public String getTopic() {
        return topic;
    }

    public void setTopic(String topic) {
        this.topic = topic;
    }

    public Integer getPartition() {
        return partition;
    }

    public void setPartition(Integer partition) {
        this.partition = partition;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimestampType() {
        return timestampType;
    }

    public void setTimestampType(String timestampType) {
        this.timestampType = timestampType;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        byte[] decodedBytes = Base64.getDecoder().decode(value);
        this.value = new String(decodedBytes);
    }

    public List<String> getHeaders() {
        return headers;
    }

    public void setHeaders(List<String> headers) {
        this.headers = headers;
    }

    public ConsumerRecord<String, String> parseToConsumerRecord() {
        return new ConsumerRecord(this.topic, this.partition, this.offset, this.timestamp,
                TimestampType.valueOf(this.timestampType), 0, 0, 0, this.key, this.value);
    }

    @Override
    public String toString() {
        return "KafkaRecord{" +
                "topic='" + topic + '\'' +
                ", partition=" + partition +
                ", offset=" + offset +
                ", timestamp=" + timestamp +
                ", timestampType='" + timestampType + '\'' +
                ", key='" + key + '\'' +
                ", value='" + value + '\'' +
                ", headers=" + headers +
                '}';
    }
}