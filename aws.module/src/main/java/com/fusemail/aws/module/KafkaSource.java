package com.fusemail.aws.module;

import com.fusemail.common.module.util.JsonUtil;
import com.fusemail.kafka.module.model.LogEvent;
import com.fusemail.kafka.module.source.LogEventSource;
import com.fusemail.kafka.module.source.MessageSource;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;

import java.util.Map;

public class KafkaSource implements RequestHandler<Map<String, String>, Object> {

    private final static String KAFKA_BOOSTRAP_HOST = "KAFKA_BOOSTRAP_HOST";

    @Override
    public Object handleRequest(Map<String, String> payload, Context context) {

        String host = System.getenv().get(KAFKA_BOOSTRAP_HOST);

        MessageSource<LogEvent> source = new LogEventSource(host);

        LogEvent logEvent = null;
        try {
            logEvent = JsonUtil.fromMapToClass(payload, LogEvent.class);
            source.sendMessage(logEvent);
        } catch (Exception e) {
            System.out.println("Error while handling log event: "+e.getMessage());
        }


        return null;
    }
}
