package com.fusemail.kafka.module.sink;

import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class MultiTopicMonitor implements TopicMonitor {

    private Logger logger = LoggerFactory.getLogger(MultiTopicMonitor.class);

    //Consuming utils
    private final String CONSUMER_ID;
    private KafkaConsumer<String, String> consumer;
    private Map<String, MessageSink> messageSinkers = new HashMap<>();

    //Thread utils
    boolean isConsuming = false;

    public MultiTopicMonitor(String groupId, String consumerId) {

        this.CONSUMER_ID = consumerId;

        Properties props = new Properties();
        props.setProperty(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "127.0.0.1:9092");
        props.setProperty(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.setProperty(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class.getName());
        props.setProperty(ConsumerConfig.GROUP_ID_CONFIG, groupId);
        props.setProperty(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
        props.setProperty(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, "false");
        props.setProperty(ConsumerConfig.MAX_POLL_RECORDS_CONFIG, "10"); //Maximum of 10 record per batch
        consumer = new KafkaConsumer<>(props);
    }

    @Override
    public void run() {
        if (isConsuming) {
            return;
        }

        isConsuming = true;
        try {

            while (isConsuming) {

                ConsumerRecords<String, String> records = consumer.poll(Duration.ofMillis(100));

                logger.info("Received {} records", records.count());
                try {
                    for (ConsumerRecord<String, String> record : records) {
                        if (!messageSinkers.containsKey(record.topic())) {
                            logger.warn("No handler for topic {}. Record discarted", record.topic());
                            continue;
                        }

                        MessageSink handler = messageSinkers.get(record.topic());
                        logger.info("======================= NEW RECORD FROM KAFKA =======================");
                        logger.info("CONSUMER:  {}", CONSUMER_ID);
                        logger.info("RECORD: Topic:{} --- Key: {} --- Value: {}", record.topic(), record.key(), record.value());
                        logger.info("============================================================================= ");
                        handler.precessRecord(record);
                    }
                    consumer.commitSync();
                    sleep(10);
                }catch (Exception e){
                    logger.error("Error when processing batch. Wont be commited");
                }
                sleep(100);
            }

        } catch (Exception e) {
            logger.error("Error in the consumer", e);
            close();
        }

    }

    private void sleep(int ms){
        try {
            Thread.sleep(ms);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void subscribeToTopic(String topic, MessageSink sink) {
        subscribeToTopics(Collections.singletonList(topic), sink);
    }

    @Override
    public void subscribeToTopics(List<String> topics, MessageSink sink) {
        for (String topic : topics){
            messageSinkers.put(topic, sink);
        }
        consumer.subscribe(topics);
    }

    public void close() {
        isConsuming = false;
        consumer.close();
    }

}
