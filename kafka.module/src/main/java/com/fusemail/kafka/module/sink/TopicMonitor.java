package com.fusemail.kafka.module.sink;

import java.util.List;

public interface TopicMonitor  extends Runnable {

    void subscribeToTopic(String topic, MessageSink sink);

    void subscribeToTopics(List<String> topic, MessageSink sink);

    void close();
}
