package com.fusemail.kafka.module.sink;

import com.fusemail.common.module.elasticsearch.model.ElasticSearchLogEvent;
import com.fusemail.common.module.elasticsearch.util.ElasticSearchUtil;
import com.fusemail.common.module.util.JsonUtil;
import com.fusemail.kafka.module.model.LogEvent;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.elasticsearch.action.index.IndexResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ElasticSearchSink implements MessageSink<String, String> {

    private Logger logger = LoggerFactory.getLogger(ElasticSearchSink.class);

    private ElasticSearchUtil elasticSearchUtil;

    public ElasticSearchSink(String indexName, String host, int ip){
        elasticSearchUtil = new ElasticSearchUtil(indexName, host, ip);
    }

    @Override
    public ConsumerRecord<String, String> precessRecord(ConsumerRecord<String, String> record) {

        IndexResponse response = null;
        try {

            LogEvent logEvent = JsonUtil.fromStringToClass(record.value(), LogEvent.class);
            ElasticSearchLogEvent elasticSearchLogEvent = new ElasticSearchLogEvent();
            elasticSearchLogEvent.setCustomerId(logEvent.getCustomerId());
            elasticSearchLogEvent.setContent(logEvent.getContent());
            //A generic ID may be created using kafka record info
//            String id = record.topic() + record.partition() + record.offset(); // this combination is unique for each record
            elasticSearchLogEvent.setIndexId(logEvent.getLogId().toString());

            response = elasticSearchUtil.putEventLog(elasticSearchLogEvent);
        } catch (Exception e) {
            logger.error("Error while sending record to ElastiSearch: {}", e.getMessage(), e);
            return null;
        }

        System.out.println("======================= Log Inserted in ElasticSearch =======================");
        System.out.println(String.format("RECORD: Index:%s --- Type: %s --- ID: %s",
                response.getIndex(), response.getType(), response.getId()));
        System.out.println("============================================================================= ");
//        logger.info("======================= Log Inserted in ElasticSearch =======================");
//        logger.info("RECORD: Index:{} --- Type: {} --- ID: {}",
//                response.getIndex(), response.getType(), response.getId());
//        logger.info("============================================================================= ");

        return record;
    }
    
}
