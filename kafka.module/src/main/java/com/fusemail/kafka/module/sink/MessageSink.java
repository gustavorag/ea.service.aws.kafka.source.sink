package com.fusemail.kafka.module.sink;

import org.apache.kafka.clients.consumer.ConsumerRecord;

public interface MessageSink<K, V> {

    //Should return the incoming record. May be just a pass through
    ConsumerRecord<K, V> precessRecord(ConsumerRecord<K, V> record);
}