package com.fusemail.kafka.module.source;

import com.fasterxml.jackson.core.JsonProcessingException;


public interface MessageSource<T> {

    void sendMessage(T message) throws Exception;

    void close();

}
