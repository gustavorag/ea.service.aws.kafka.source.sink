package com.fusemail.kafka.module.source;

import com.fusemail.common.module.util.JsonUtil;
import com.fusemail.kafka.module.model.LogEvent;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Properties;

public class LogEventSource implements MessageSource<LogEvent> {

    private static Logger logger = LoggerFactory.getLogger(LogEventSource.class);
    private final String TOPIC = "log_event";
    private final int SIZE_32_KB = 32*1024;
    private KafkaProducer<String, String> producer;

    public LogEventSource(String boostrapHost) {

        Properties props = new Properties();
        props.setProperty(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, boostrapHost);
        props.setProperty(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());
        props.setProperty(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class.getName());

        //to make it safer and improve performance
        props.setProperty(ProducerConfig.ENABLE_IDEMPOTENCE_CONFIG, "true");
        props.setProperty(ProducerConfig.ACKS_CONFIG, "all");
        props.setProperty(ProducerConfig.RETRIES_CONFIG, String.valueOf(Integer.MAX_VALUE));
        props.setProperty(ProducerConfig.MAX_IN_FLIGHT_REQUESTS_PER_CONNECTION, "5");

        //For high throughput producer (at the expense of a bit of latency and CPU usage - compressing)
        //Snappy - efficient compression for JSON or log texts
        props.setProperty(ProducerConfig.COMPRESSION_TYPE_CONFIG, "snappy");

        //Linger MS = How long will the producer wait to send the batch out. Lets say the we set this value to 100 ms. Then producer sends a message.
        //It will wait 100 ms before send the record to Kafka. If during those 100 ms another messages are produced, then all records within the 100ms interval
        // will be send in the same batch.
        props.setProperty(ProducerConfig.LINGER_MS_CONFIG, "20");

        //If batch size (in KB) within the LINGER MS reach this size, the batch will be sent out. If a single record is bigger than the batch size,
        //it will not be sent
        props.setProperty(ProducerConfig.BATCH_SIZE_CONFIG, String.valueOf(SIZE_32_KB));

        producer = new KafkaProducer<>(props);
    }

    @Override
    public void sendMessage(LogEvent message) throws Exception {

        if (message.getCustomerId() == null) {
            throw new Exception("customer id is required");
        }

        //Creates a unique identifier for the record being inserted in Kafka so it can be used to avoid duplicates
        //Other option is at the Consumer level. See ElasticSearchHandler
        //message.setEventId(UuidUtil.getSecureRandomUUID());

        String customerId = message.getCustomerId().toString();
        ProducerRecord<String, String> record = new ProducerRecord<>(TOPIC, customerId, JsonUtil.objectToString(message));
        producer.send(record, new Callback() {
            @Override
            public void onCompletion(RecordMetadata recordMetadata, Exception e) {

                if (e != null) {
                    logger.error("Error while sending Kafka Record [{}]", recordMetadata.toString(), e);
                    return;
                }

                logger.info("================= Record sent =================");
                logger.info("Topic: {} --- Partition: {} --- Offset: {} --- Timestamp: {}",
                        recordMetadata.topic(), recordMetadata.partition(),
                        recordMetadata.offset(), recordMetadata.timestamp());
                logger.info("===============================================");
            }
        });
        producer.flush();
    }

    @Override
    public void close() {
        producer.close();
    }
}
