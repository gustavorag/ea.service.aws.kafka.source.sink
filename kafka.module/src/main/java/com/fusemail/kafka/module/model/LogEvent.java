package com.fusemail.kafka.module.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonIgnoreProperties(ignoreUnknown = true)
@JsonPropertyOrder({
        "customer_id",
        "type",
        "content",
})
public class LogEvent {

    @JsonProperty("log_id")
    private Long logId;

    @JsonProperty("customer_id")
    private Long customerId;

    @JsonProperty("type")
    private String type;

    @JsonProperty("content")
    private String content;

    public Long getLogId() {
        return logId;
    }

    public void setLogId(Long logId) {
        this.logId = logId;
    }

    public Long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Long customerId) {
        this.customerId = customerId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public String toString() {
        return "LogEvent{" +
                "logId=" + logId +
                ", customerId=" + customerId +
                ", type='" + type + '\'' +
                ", content='" + content + '\'' +
                '}';
    }
}
