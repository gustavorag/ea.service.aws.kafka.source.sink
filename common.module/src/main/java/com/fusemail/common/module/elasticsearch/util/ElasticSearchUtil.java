package com.fusemail.common.module.elasticsearch.util;

import com.fusemail.common.module.elasticsearch.model.ElasticSearchLogEvent;
import com.fusemail.common.module.util.JsonUtil;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.apache.http.HttpStatus;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.common.xcontent.XContentType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ElasticSearchUtil {

    private static Logger logger = LoggerFactory.getLogger(ElasticSearchUtil.class);

    private RestHighLevelClient client;
    private final String indexName;

    public ElasticSearchUtil(String indexName, String elasticSearchHost, int elasticSearchIp) {

        this.indexName = indexName;

        RestClientBuilder builder = RestClient.builder(new HttpHost(elasticSearchHost, elasticSearchIp));
        client = new RestHighLevelClient(builder);
    }

    public IndexResponse putEventLog(ElasticSearchLogEvent logEvent) throws Exception {

        String content = JsonUtil.objectToString(logEvent);

        if (StringUtils.isBlank(logEvent.getIndexId())) {
            logger.warn("ElasticSearch Log event {} has no unique id. It will be discarded.", content);
        }

        //Creating index with ID so if the specific event already exists in ElasticSearch, it will be updated
        IndexRequest indexRequest = new IndexRequest(indexName)
                .id(logEvent.getIndexId())
                .source(content, XContentType.JSON);

        IndexResponse response = client.index(indexRequest, RequestOptions.DEFAULT);
        if (HttpStatus.SC_CREATED != response.status().getStatus()) {
            throw new Exception(String.format("Error while creating content to index [%s] - error [%s]",
                    indexName, response.status().toString()));
        }

        return response;

    }
}
