package com.fusemail.common.module.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.Map;

public class JsonUtil {

    private static ObjectMapper objectMapper = new ObjectMapper();

    public static String objectToString(Object value) throws JsonProcessingException {

        return objectMapper.writeValueAsString(value);
    }

    public static <T> T fromStringToClass(String content, Class<T> clazz) throws JsonProcessingException {
        return objectMapper.readValue(content, clazz);
    }

    public static <T> T fromMapToClass(Map content, Class<T> clazz) throws JsonProcessingException {
        String asString = JsonUtil.objectToString(content);
        return JsonUtil.fromStringToClass(asString, clazz);
    }
}
