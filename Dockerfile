ARG registry=docker-registry.electric.net:10080
ARG build_env=4.0.0-java11-maven3.6.3
ARG run_env=11-jre-slim

FROM ${registry}/fusemail/fm-utility-java-build:${build_env} as builder

ARG project_version=master
ARG git_hash
ARG build_time

RUN mkdir -p /usr/local/src/ea.service.aws.kafka.source.sink
WORKDIR /usr/local/src/ea.service.aws.kafka.source.sink
COPY --chown=container . /usr/local/src/ea.service.aws.kafka.source.sink

RUN mvn clean package -B --settings=/.m2/maven-settings.xml -Duser.timezone=America/Vancouver -Dversion=${project_version} -Dgit.hash=${git_hash} -Dbuild.timestamp=${build_time}

FROM ${registry}/openjdk/openjdk:${run_env}
RUN apt-get update -y && apt-get install -y curl

COPY --from=builder /usr/local/src/ea.service.aws.kafka.source.sink/target/ea.service.aws.kafka.source.sink-*.jar /usr/share/fusemail/ea.service.aws.kafka.source.sink/ea.service.aws.kafka.source.sink.jar
COPY --from=builder /usr/local/src/ea.service.aws.kafka.source.sink/config/apptemplate.properties /etc/fusemail/ea.service.aws.kafka.source.sink/apptemplate.properties
COPY --from=builder /usr/local/src/ea.service.aws.kafka.source.sink/config/log4j.xml /etc/fusemail/ea.service.aws.kafka.source.sink/log4j.xml

ENTRYPOINT ["java"]

CMD ["-Xmx128m", \
    "-XX:+UseG1GC", \
    "-Dlog4j.configurationFile=file:/etc/fusemail/ea.service.aws.kafka.source.sink/log4j.xml", \
    "-Dapptemplate.properties=/etc/fusemail/ea.service.aws.kafka.source.sink/apptemplate.properties", \
    "-jar", \
    "/usr/share/fusemail/ea.service.aws.kafka.source.sink/ea.service.aws.kafka.source.sink.jar"]
