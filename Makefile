SHELL := /bin/bash
################################################################
# Makefile for build, test, and publish project related files
#
# Run examples (see more commands in "help" target):
# $ make              -- build final docker run image..
# $ make clean        -- removes all project related built files.
# $ make docker-push  -- tag and push latest docker image to registry.
# $ make docker-test  -- run test use docker container.
################################################################

#########################################
# Package Metadata
#########################################
REPO_NAME=ea.service.aws.kafka.source.sink
PROJECT_NAME=$(REPO_NAME)
VERSION=$(shell git describe --tags --always)
GITBRANCH=$(shell git rev-parse --abbrev-ref HEAD | grep -oE "^(master|dev|release-.+|[A-Z]+-[0-9]+|QA)")
GITHASH=$(shell git rev-parse --short HEAD)
BUILDDATE=$(shell date '+%Y-%m-%d_%H:%M:%S_%Z')

#########################################
# CI/CD Config
#########################################
# registry UI available here: https://bamboo2203a.electric.net:10081/repository/
REGISTRY_ADDR=docker-registry.electric.net:10080
BUILD_ENV=4.0.0-java11-maven3.6.3
RUN_ENV=11-jre-slim

ifeq ($(GITBRANCH),master)
	BUILDTAG=$(VERSION)
	NOMAD_NAME=$(REPO_NAME)
else
	BUILDTAG=$(VERSION)-$(GITBRANCH)
	NOMAD_NAME=$(REPO_NAME)-$(GITBRANCH)
endif


.PHONY: clean help docker docker-build docker-push docker-test docker-test-clean docker-push-clean docker-run docker-run-stop docker-run-logs

#########################################
# Docker Targets
#########################################
docker: build/docker

build/docker:
	@mkdir -p build && \
	docker build --build-arg=project_version=$(BUILDTAG) --build-arg=git_hash=$(GITHASH) --build-arg=build_time=$(BUILDDATE) --build-arg=registry=$(REGISTRY_ADDR) --build-arg=build_env=$(BUILD_ENV) --build-arg=run_env=$(RUN_ENV) -t fusemail/$(PROJECT_NAME):$(BUILDTAG) -t fusemail/$(PROJECT_NAME):latest . && \
	touch build/docker

docker-build: build/docker_build

build/docker_build:
	@mkdir -p build && \
	docker build --build-arg=project_version=$(BUILDTAG) --build-arg=git_hash=$(GITHASH) --build-arg=build_time=$(BUILDDATE) --build-arg=registry=$(REGISTRY_ADDR) --build-arg=build_env=$(BUILD_ENV) --build-arg=run_env=$(RUN_ENV) --target builder -t fusemail/$(PROJECT_NAME):$(BUILDTAG)-build . && \
	touch build/docker_build

docker-compile: docker-build
	CONTAINER=$$(docker create fusemail/$(PROJECT_NAME):$(BUILDTAG)-build mvn compile -B --settings /.m2/maven-settings.xml -Duser.timezone=America/Vancouver -Dversion=$(BUILDTAG)); \
	docker start -a $$CONTAINER; \
	docker cp $$CONTAINER:/usr/local/src/$(PROJECT_NAME)/target/. target; \
	docker rm $$CONTAINER

docker-raml: docker-build
	CONTAINER=$$(docker create fusemail/$(PROJECT_NAME):$(BUILDTAG)-build mvn antrun:run@build-raml -B --settings /.m2/maven-settings.xml -Duser.timezone=America/Vancouver -Dversion=$(BUILDTAG)); \
	docker start -a $$CONTAINER; \
	docker cp $$CONTAINER:/usr/local/src/$(PROJECT_NAME)/doc/dist/. doc/dist; \
	docker rm $$CONTAINER

docker-sonar-push: docker-build
	if [ "$(GITBRANCH)" = "master" ]; then \
		docker run --rm fusemail/$(PROJECT_NAME):$(BUILDTAG)-build mvn clean package sonar:sonar -B --settings /.m2/maven-settings.xml -Duser.timezone=America/Vancouver -Dversion=$(BUILDTAG) -Dsonar.projectKey=$(PROJECT_NAME); \
	fi

docker-push: docker
	docker tag fusemail/$(PROJECT_NAME):$(BUILDTAG) $(REGISTRY_ADDR)/fusemail/$(PROJECT_NAME):$(BUILDTAG) && \
	docker push $(REGISTRY_ADDR)/fusemail/$(PROJECT_NAME):$(BUILDTAG) && \
	if [ "$(GITBRANCH)" = "master" ]; then \
		docker tag fusemail/$(PROJECT_NAME):latest $(REGISTRY_ADDR)/fusemail/$(PROJECT_NAME):latest && \
		docker push $(REGISTRY_ADDR)/fusemail/$(PROJECT_NAME):latest; \
	elif [ "$(GITBRANCH)" = "dev" ]; then \
		docker tag fusemail/$(PROJECT_NAME):$(BUILDTAG) $(REGISTRY_ADDR)/fusemail/$(PROJECT_NAME):$(GITBRANCH) && \
		docker push $(REGISTRY_ADDR)/fusemail/$(PROJECT_NAME):$(GITBRANCH); \
		docker tag fusemail/$(PROJECT_NAME):$(BUILDTAG) $(REGISTRY_ADDR)/fusemail/$(PROJECT_NAME):$(GITBRANCH)-$(GITHASH) && \
		docker push $(REGISTRY_ADDR)/fusemail/$(PROJECT_NAME):$(GITBRANCH)-$(GITHASH); \
	fi

docker-test: docker docker-build
	@mkdir -p test-result
	export PROJECT_NAME=$(PROJECT_NAME); \
	export BUILDTAG=$(BUILDTAG); \
	docker-compose down && \
	docker-compose up --build end2end-test; \
	CONTAINER=$$(docker-compose ps -q end2end-test); \
	docker cp $$CONTAINER:/usr/local/src/$(PROJECT_NAME)/target/surefire-reports/. ./test-result; \
	docker-compose down -v

docker-run: docker docker-run-stop
	docker run -d --rm --name $(PROJECT_NAME)-$(BUILDTAG) -e DATACENTER="devel" -p 8080:8080 fusemail/$(PROJECT_NAME):latest

docker-run-logs: docker-run
	docker logs -f $(PROJECT_NAME)-$(BUILDTAG)

docker-run-stop:
	-docker rm -f $(PROJECT_NAME)-$(BUILDTAG)

docker-test-clean:
	-docker rmi -f fusemail/$(PROJECT_NAME):$(BUILDTAG)-build
	rm -rf build/docker_build
	rm -rf build/docker
	rm -rf test-result

docker-push-clean:
	-docker rmi -f $(REGISTRY_ADDR)/fusemail/$(PROJECT_NAME):$(BUILDTAG)
	-docker rmi -f $(REGISTRY_ADDR)/fusemail/$(PROJECT_NAME):latest
	-docker rmi -f fusemail/$(PROJECT_NAME):$(BUILDTAG)
	-docker rmi -f fusemail/$(PROJECT_NAME):latest
	rm -rf build/docker

#########################################
# Deploy Targets by pushing to artifact server.
# Only applicable running directly from Bamboo.
#########################################
jobspec-tmpl:
	cd config && \
	datacentres=( "fusepoint" "ireland" "london" "sweden1" "sweden2" "toronto" ) && \
	for dc in "$${datacentres[@]}"; do \
		sed -e "s/devel/$$dc/g; s/\"-e\", \"dev\"/\"-e\", \"prod\"/" < jobspec-devel.hcl > "jobspec-$$dc.hcl"; \
	done

jobspec:
	@mkdir -p build
	for f in config/*.hcl; do \
		name=$$(basename -- "$$f") && \
		sed -e "s/###BUILDTAG###/$(BUILDTAG)/; s/###NOMAD_NAME###/$(NOMAD_NAME)/; s/###REPO_NAME###/$(REPO_NAME)/" < "$$f" > "build/$$name"; \
	done

deploy-clean: docker-push-clean
	if [ -d "build/" ]; then rm -rf build; fi

deploy: docker-push docker-sonar-push jobspec
	cd build && artifact new_version $(PROJECT_NAME) $(BUILDTAG) || true && \
	for f in *.hcl; do \
		artifact push $(PROJECT_NAME) $(BUILDTAG) "$$f"; \
	done && \
	if [ "$(GITBRANCH)" = "master" ]; then \
		artifact tag $(PROJECT_NAME) $(BUILDTAG) latest; \
	fi && \
	for f in *.hcl; do \
		dc=$$(echo "$$f" | sed 's/^jobspec-\(.*\).hcl/\1/') && \
		if [ "$$dc" = "devel" ] || [ "$(GITBRANCH)" = "master" ]; then \
			curl -XPOST -d"{\"name\":\"$(PROJECT_NAME)\",\"version\":\"$(BUILDTAG)\"}" "http://localhost:4151/pub?topic=bamboo_$${dc}_deployment"; \
		fi; \
	done

#########################################
# Cleanup Target
#########################################
clean: docker-test-clean docker-push-clean
	rm -rf build
	rm -rf target
	rm -rf doc/dist

#########################################
# Help
#########################################
help:
	@echo " $ make docker	-- build final docker run image."
	@echo " $ make docker-push	-- tag and push latest docker image to registry."
	@echo " $ make docker-test	-- run test use docker container."
	@echo " $ make docker-test-clean	-- remove test related files."
	@echo " $ make docker-push-clean  -- remove deploy related files."
	@echo " $ make docker-run	-- run the built docker image."
	@echo " $ make docker-run-logs	-- run and tail the logs of the serivce inside the container "
	@echo " $ make docker-run-stop	-- force stop and remove the container created by the 'docker-run' target."
	@echo " $ make clean	-- removes all project related build/test files."
